const router = require('express').Router()
const multer = require('multer')
let Song = require('../models/song.models')
let Singer = require('../models/singer.models')
let GroupBand = require('../models/groupBand.models')

const Mongoose = require('mongoose')
const ObjectId = Mongoose.Types.ObjectId

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/mp3/')
  },
  fileName: (req, file, cb) => {
    cb(null, new Date().toISOString() + file.originalname)
  }
})

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'audio/mpeg') {
    cb(null, true)
  } else {
    cb(new Error("You can only upload mp3 file"), false)
  }
}
const mp3 = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 10
  },
  fileFilter: fileFilter
})

// GET ALL
router.get('/', async (req, res) => {
  try {
    let song = await Song.find()
    res.json(song)
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
//GET BY ID
router.get('/:id', getId, (req, res) => {
  Song.findById(req.params.id)
    .then(song => res.json(song))
    .catch(err => res.status(400).json("Error" + err))
})

// ADD SONG WITH SOLO SINGER W/O FEATURE SINGER = OK
router.post('/addSoloSong', mp3.single('mp3File'), async (req, res) => {
  // console.log(req.file)
  // console.log(req.body.singer);

  const newSong = new Song({
    title: req.body.title,
    singer: singersModel,
    releaseDate: Date.singersModel,
    mp3File: req.file.path,
    createdAt: new Date()
  })
  try {
    let songAdded = await newSong.save()
    res.status(201).json(songAdded)
  } catch (err) {
    console.log(err);

    res.status(400).json(err) //{ message: err.message }
    // 400 - something wrong with user input
  }
})

// ADD SONG WITH SOLO SINGER W FEATURE SINGER = OK
router.post('/addSoloSongWithFs', mp3.single('mp3File'), async (req, res) => {  //
  const newSong = new Song({
    title: req.body.title,
    singer: req.body.singer,
    featureSinger: req.body.featureSinger,
    releaseDate: Date.parse(req.body.releaseDate),
    mp3File: req.file.path,
    createdAt: new Date()
  })

  try {
    let songAdded = await newSong.save()
    res.status(201).json(songAdded)
  } catch (err) {
    console.log(err);

    res.status(400).json({ message: err.message })
    // 400 - something wrong with user input
  }
})

// ADD SONG WITH GroupBand With Feature Singer = OK
router.post('/addGroupSongWithFs', mp3.single('mp3File'), async (req, res) => {

  let groupBand = await GroupBand.findOne({ _id: ObjectId(req.body.groupBand) })
  const newSong = new Song({
    title: req.body.title,
    groupBand: groupBand,
    featureSinger: ObjectId(req.body.featureSinger),
    releaseDate: Date.parse(req.body.releaseDate),
    mp3File: req.file.path,
    createdAt: new Date()
  })
  try {
    let songAdded = await newSong.save()
    res.status(201).json(songAdded)
  } catch (err) {
    res.status(400).json({ message: err.message })
    // 400 - something wrong with user input
  }
})

// ADD SONG WITH GroupBand = OK then populate groupBand = OK
router.post('/addGroupSong', mp3.single('mp3File'), async (req, res) => {
  let group = req.body.groupBand
  let groupBand = await GroupBand.find({ '_id': { $in: ObjectId(group) } }).populate('singer')

  const newSong = new Song({
    title: req.body.title,
    groupBand: groupBand,
    releaseDate: Date.parse(req.body.releaseDate),
    mp3File: req.file.path,
    createdAt: new Date()
  })
  try {
    let songAdded = await newSong.save()
    res.status(201).json(songAdded)
  } catch (err) {
    res.status(400).json({ message: err.message })
    // 400 - something wrong with user input
  }
})

// Update All by ID
router.patch('/update/:id', getId, (req, res) => {
  Song.findById(req.params.id)
    .then(songs => {
      songs.song = req.body.song,
        songs.groupSingers = req.body.groupSingers,
        songs.featureSinger = req.body.featureSinger,
        songs.releaseDate = Date.parse(req.body.data),
        songs.mp3File = req.file.path,
        songs.updatedAt = req.file.updatedAt

      songs.save()
        .then(() => res.status(201).json('Song updated'))
        .catch((err) => res.status(400).json('Error: ' + err))
    })
    .catch(err => res.status(400).json('Error: ' + err))
})

router.delete('/delete/:id', getId, (req, res) => {
  Song.findByIdAndDelete(req.params.id)
    .then(song => res.json(song))
    .catch(err => res.status(400).json("Error" + err))
})

async function getId(req, res, next) {
  let id
  try {
    id = await Song.findById(req.params.id)
    if (id === null) {
      return res.status(404).json({ message: 'Cannot find song' })
    }
  } catch (err) {
    res.status(500).status({ message: err.message })
  }
  res.id = id
  next()
}
module.exports = router