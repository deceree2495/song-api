const router = require("express").Router();
const GroupBand = require('../models/groupBand.models')
const Mongoose = require('mongoose')
const ObjectId = Mongoose.Types.ObjectId
const Singer = require('../models/singer.models')

router.get("/", async (req, res) => {
  try {
    let allBand = await GroupBand.find();
    res.json(allBand);
  } catch (err) {
    console.log(err);
    res.status(404).json({ message: err.message });
  }
});

router.get("/:bandId", async (req, res) => {
  GroupBand.findById(req.params.bandId)
    .then(band => res.json(band))
    .catch(err => {
      console.log(err);
      res.status(404).json("Error" + err);
    });
});

// OK
router.post("/addBand", async (req, res) => {
  let singers = req.body.singer

  let singerArr = singers.split(', ')
  let singerArrId = singerArr.map(ele => ObjectId(ele))
  let singer = await Singer.find({ '_id': { $in: singerArrId } })

  const addedGroup = new GroupBand({
    bandName: req.body.bandName,
    singer: singer,
    debutDate: Date.parse(req.body.debutDate)
  })

  try {
    const group = await addedGroup.save();
    res.status(201).json(group);
  } catch (err) {
    console.log(err);
    res.status(400).json(err);
  }
});

// router.put('/bandId', async (req, res) => {
//   const
// })

// ADD GROUP BAND
// router.post('/addGroupBand', async (req, res) => {  //
//   const newGroup = new Song({
//     groupSingers: req.body.groupSingers,
//     featureSinger: req.body.featureSinger,
//     releaseDate: Date.parse(req.body.releaseDate),
//     mp3File: req.file.path,
//   })
//   try {
//     let songAdded = await newSong.save()
//     res.status(201).json(songAdded)
//   } catch (err) {
//     res.status(400).json({ message: err.message })
//     // 400 - something wrong with user input
//   }
// })


module.exports = router;
