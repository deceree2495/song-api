const router = require("express").Router();
const Mongoose = require('mongoose')
const ObjectId = Mongoose.Types.ObjectId
const bcrypt = require('bcrypt')

const User = require('../models/user.models')

router.get("/", async (req, res) => {
  try {
    let user = await User.find();
    res.json(user);
  } catch (err) {
    console.log(err);
    res.status(404).json({ message: err.message });
  }
});

router.get("/:userId", async (req, res) => {
  User.findById(req.params.userId)
    .then(user => res.json(user))
    .catch(err => {
      console.log(err);
      res.status(404).json("Error" + err);
    });
});

// register
router.post("/addUser", async (req, res) => {

  const newUser = new User({
    name: {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
    },
    email: req.body.email,
    password: req.body.password,// encrypt, = 1234567
    createdAt: new Date()
  })

  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newUser.password, salt, (err, hash) => {
      if (err) {
        console.log(err)
      }
      newUser.password = hash
      console.log(newUser);

      newUser.save(err => {
        if (!err) {
          res.status(201).json(newUser)
        } else {
          res.status(500).json({ message: err.message })
        }
      })
    })
  })
});


module.exports = router;
