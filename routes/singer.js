const router = require("express").Router();
const Singer = require('../models/singer.models');

router.get("/", async (req, res) => {
  try {
    let singer = await Singer.find();
    res.json(singer);
  } catch (err) {
    res.status(500).json({ message: err.message });
    console.log(err);
  }
});

router.post("/addSinger", async (req, res) => {
  console.log(req.body.name);

  let newSinger = new Singer()
  newSinger.name = req.body.name
  newSinger.createdAt = new Date()

  newSinger.save(err => {
    if (err) {
      res.status(400).json({ message: err.message })
    }
    res.json(newSinger)
  })

  // const newSinger = new Singer({
  //   name: req.body.name,
  //   createdAt: new Date()
  // })
  // try {
  //   let singerAdded = await newSinger.save();
  //   res.status(201).json(singerAdded);
  // } catch (err) {
  //   res.status(400).json({ message: err.message });
  //   console.log(err);
  // }
});

module.exports = router;
