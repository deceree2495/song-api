require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3335 || 5000; //process.env.PORT

app.use(express.json());
app.use(express.static("public")); // for multer
app.use(express.urlencoded({ extended: true }))

const url = process.env.MONGO_URL;
mongoose.connect(url, { useCreateIndex: true, useNewUrlParser: true });
const db = mongoose.connection;
db.on("error", error => console.log(error));
db.once("open", () => console.log("Connected to database"));

const userRouter = require("./routes/user");
const songRouter = require("./routes/song");
const singerRouter = require("./routes/singer");
const groupBandRouter = require("./routes/groupBand");

app.use("/user", userRouter);
app.use("/song", songRouter);
app.use("/singer", singerRouter);
app.use("/groupBand", groupBandRouter);

// 400 Error setup
app.use((req, res, next) => {
  const error = new Error("Not Found");
  error.status(400);
  next(error);
});
// ============================================= //
// 500 Error setup
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});
