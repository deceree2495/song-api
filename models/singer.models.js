const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const singerSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  groupBand: {
    type: Schema.Types.ObjectId,
    ref: "Singer"
  },
  createdAt: {
    type: Date,
    default: null
  },
  updatedAt: {
    type: Date,
    default: null
  },
  deletedAt: {
    type: Date,
    default: null
  },
  assignedAt: {
    type: Date,
    default: null
  }
});

const Singer = mongoose.model("Singer", singerSchema);

module.exports = Singer;
