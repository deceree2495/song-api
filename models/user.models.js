const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  name: {
    firstName: String,
    lastName: String,
    middleName: String
  },
  fullName: String,
  email: String,
  password: String,
  createdAt: {
    type: Date,
    default: null
  },
  updatedAt: {
    type: Date,
    default: null
  },
  deletedAt: {
    type: Date,
    default: null
  },
  assignedAt: {
    type: Date,
    default: null
  }
})

const User = mongoose.model('User', userSchema)

module.exports = User