const router = require('express').Router()
const multer = require('multer')
let Song = require('../models/song.models')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/mp3/')
    },
    fileName: (req, file, cb) => {
        cb(null, new Date().toISOString() + file.originalname)
    }
})
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'audio/mpeg') {
        cb(null, true)
    } else {
        cb(new Error("You can only upload mp3 file"), false)
    }
}
const mp3 = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 10
    },
    fileFilter: fileFilter
})

router.get('/', async (req, res) => {
    try {
        let song = await Song.find()
        res.json(song)
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})
router.get('/:id', getId, (req, res) => {
    Song.findById(req.params.id)
        .then(song => res.json(song))
        .catch(err => res.status(400).json("Error" + err))
})

router.post('/addGroupSinger', mp3.single('mp3File'), async (req, res) => {  //
    console.log(req.file)
    const newSong = new Song({
        song: req.body.song,
        groupSingers: req.body.groupSingers,
        featureSinger: req.body.featureSinger,
        releaseDate: Date.parse(req.body.releaseDate),
        mp3File: req.file.path,
        // coverImage: req.file.coverImage //
    })
    try {
        let songAdded = await newSong.save()
        res.status(201).json(songAdded)
    } catch (err) {
        res.status(400).json({ message: err.message })
        // 400 - something wrong with user input
    }
})

router.patch('/update/:id', getId, (req, res) => {
    Song.findById(req.params.id)
        .then(songs => {
            songs.song = req.body.song,
                songs.groupSingers = req.body.groupSingers,
                songs.featureSinger = req.body.featureSinger,
                songs.releaseDate = Date.parse(req.body.data),
                songs.mp3File = req.file.path

            songs.save()
                .then(() => res.status(201).json('Song updated'))
                .catch((err) => res.status(400).json('Error: ' + err))
        })
        .catch(err => res.status(400).json('Error: ' + err))
})
router.delete('/delete/:id', getId, (req, res) => {
    Song.findByIdAndDelete(req.params.id)
        .then(song => res.json(song))
        .catch(err => res.status(400).json("Error" + err))
})

async function getId(req, res, next) {
    let id
    try {
        id = await Song.findById(req.params.id)
        if (id === null) {
            return res.status(404).json({ message: 'Cannot find song' })
        }
    } catch (err) {
        res.status(500).status({ message: err.message })
    }
    res.id = id
    next()
}
module.exports = router